<?php
session_start();
function logout()
{
    session_start();
    unset($_SESSION);
    session_destroy();
    session_write_close();
}
if(isset($_POST["submit"])) {
logout();
}
?>

<header class="header container d-flex container-md container-sm d-sm-flex d-md-flex bg-secondary w-100 align-items-center ">

    <nav class="navbar navbar-dark">
        <button type="button" class="btn dropdown" data-bs-toggle="dropdown" aria-expanded="false">
            <span class="navbar-toggler-icon"></span>
        </button>
        <ul class="dropdown-menu p-0 bg-secondary mt-3">

            <li ><a class="dropdown-item " href="authorization.php">Вход</a><hr class="dropdown-divider m-0 bg-white"></li>

<form method="post">

            <input type="submit" class="dropdown-item" name="submit" value="Выход"><hr class="dropdown-divider m-0 bg-white">
</form>

            <li><a class="dropdown-item" href="navigation.php">Навигация</a><hr class="dropdown-divider m-0 bg-white"></li>

            <li><a class="dropdown-item" href="view.php">Просмотр</a><hr class="dropdown-divider m-0 bg-white"></li>

            <li><a class="dropdown-item" href="add.php">Добавление</a></li>
        </ul>
    </nav>
    <label><a class="text-light fs-4 text-decoration-none mx-3"><?if (!empty($_SESSION['login'])) echo $_SESSION['login'];?></a></label>

</header>
