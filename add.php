<?php
session_start();
include_once('connection.php');
$id_user = $_SESSION['id_user'];
$email = $_POST["email"];
$login = $_POST["login"];
$password = $_POST["password"];
$url = $_POST["url"];
$desc = $_POST["description"];

$params = [
        'id_user'=>$id_user,
    'email'=>$email,
    'login'=>$login,
    'password' => $password,
    'url'=>$url,
    'description'=>$desc
];
function dataInput(array $params): bool
{
    $mysql = connect();

    $sql = $mysql->prepare("INSERT INTO records (id_user, email, login, password, url, description) VALUES (:id_user, :email, :login, :password, :url, :description)");

    return $sql->execute($params);
}

if (!empty($_SESSION['id_user'])){
    dataInput($params);
}
else echo 'Вы не авторизированы'
?>
<!DOCTYPE html>
<html lang="en" xmlns="http://www.w3.org/1999/html">
<head>
    <meta charset="utf-8" name="viewport" content="width=device-width, initial-scale=1">
    <title>Title</title>

    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="style.css">

</head>
<body>
<?php
if (!empty($_SESSION['id_user'])){
    echo $_SESSION['id_user'];
}
?>

<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/js/bootstrap.bundle.min.js" integrity="sha384-JEW9xMcG8R+pH31jmWH6WWP0WintQrMb4s7ZOdauHnUtxwoG2vI5DkLtS3qm9Ekf" crossorigin="anonymous"></script>

    <?php require 'header.php'?>


<div class="container-md block d-md-flex justify-content-center h-100">
    <form method="post">
        <div class="mt-5 mb-3">
            <label for="email" class="form-label">Електронная почта</label>
            <input type="email" class="form-control" id="email" aria-describedby="emailHelp" name="email">

        </div>
        <div class="mb-3">
            <label for="login" class="form-label">Логин</label>
            <input type="text" class="form-control " id="login" name="login">
        </div>
        <div class="mb-3">
            <label for="password" class="form-label">Пароль</label>
            <input type="text" class="form-control " id="password" name="password">
        </div>
        <div class="mb-3">
            <label for="link" class="form-label">Ссылка на ресурс</label>
            <input type="text" class="form-control " id="link" name="url">
        </div>
        <div class="mb-3">
            <label for="description" class="form-label">Описание</label>
            <input type="text" class="form-control " id="description" name="description">
        </div>

        <button type="submit" class="btn btn-primary mt-1">Записать</button>
        <!--Кликнув на записать появляется надпись (сообщение) записано-->
    </form>
</div>




</body>
</html>