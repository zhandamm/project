<?php
session_start();
include_once('connection.php');
$userName = htmlspecialchars(trim($_POST['login']));
$password =  htmlspecialchars(trim($_POST['password']));

$params = [
    'login'=>$userName,
    'password' => $password
];
function validate(array $params)
{
    $mysql = connect();


    $sql = $mysql->prepare("select id_user, login, password from user_info where login=:login and password=:password");

    if (!empty($params['login'])&& !empty($params['password'])){

        $sql->execute($params);
        $sql = $sql->fetch(PDO::FETCH_ASSOC);
        if (empty($sql['password'])){
            echo ('користувача не існує, зареєструйтесь');
        }
        else{
            if ($sql['password']==$params['password']){
                 $_SESSION['login']= $sql['login'];
                $_SESSION['id_user']= $sql['id_user'];

            }
            else echo 'Введений вами логін або пароль є невірним';
        }
    }


    return $sql;
}


?>
<!DOCTYPE html>
<html lang="en" xmlns="http://www.w3.org/1999/html">
<head>
    <meta charset="utf-8" name="viewport" content="width=device-width, initial-scale=1">
    <title>Title</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="style.css">
</head>
<body>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/js/bootstrap.bundle.min.js" integrity="sha384-JEW9xMcG8R+pH31jmWH6WWP0WintQrMb4s7ZOdauHnUtxwoG2vI5DkLtS3qm9Ekf" crossorigin="anonymous"></script>
<?require 'header.php'?>
<div class="container-md container-sm container d-sm-flex d-flex block mt-3 d-md-flex justify-content-center align-items-center  ">
    <form method="post">
<h4 class="text-center mb-4 mt-5">Авторизация</h4><hr class="bg-secondary">

        <div class="mb-4">
            <label for="login" class="form-label">Логин</label>
            <input type="text" class="form-control " id="login" name="login">
        </div>
        <div class="mb-4">
            <label for="password" class="form-label">Пароль</label>
            <input type="text" class="form-control " id="password" name="password">
        </div>

        <button type="submit" class="btn btn-primary ">Отправить</button>
        <a href="registration.php" class="link-primary   text-decoration-none">Регистрация</a>
        <!--Кликнув на записать появляется надпись (сообщение) записано-->
    </form>
    <?validate($params);?>
</div>
</body>
</html>