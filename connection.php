<?php
function connect(): PDO
{
    static $db;

    if($db === null){
        $db = new PDO('mysql:host=localhost;dbname=mobile_project', 'mysql', 'mysql', [
            PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC
        ]);

        $db->exec('SET NAMES UTF8');
    }

    return $db;

}


function dbCheckError(PDOStatement $query) : bool{
    $errInfo = $query->errorInfo();

    if($errInfo[0] !== PDO::ERR_NONE){
        echo $errInfo[2];
        exit();
    }

    return true;
}