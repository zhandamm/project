<?php
session_start();
include_once('connection.php');
$id_user = $_SESSION['id_user'];

function view($id_user)
{
    $mysql = connect();
    $sql = $mysql->prepare("SELECT records.email, records.login, records.password, records.url, records.description FROM `records`, user_info WHERE records.id_user=user_info.id_user AND records.id_user=:id_user
");
    $sql->bindParam(':id_user', $id_user);
    $sql->execute();

    $sql = $sql->fetchAll();
    return $sql;
}
$arr = view($id_user);


?>
<!DOCTYPE html>
<html lang="en" xmlns="http://www.w3.org/1999/html">
<head>
    <meta charset="utf-8" name="viewport" content="width=device-width, initial-scale=1">
    <title>Title</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="style.css">
</head>
<body>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/js/bootstrap.bundle.min.js" integrity="sha384-JEW9xMcG8R+pH31jmWH6WWP0WintQrMb4s7ZOdauHnUtxwoG2vI5DkLtS3qm9Ekf" crossorigin="anonymous"></script>
<?require 'header.php'?>
<div class="container-md container-sm container-lg table-responsive-md table-responsive-sm table-responsive-lg d-lg-flex d-md-flex d-sm-flex d-flex justify-content-center align-items-center  ">
    <table class="table d-inline-block">

        <thead>
        <tr>
            <th scope="col">Почта</th>
            <th scope="col">Логин</th>
            <th scope="col">Пароль</th>
            <th scope="col">Описание</th>
            <th scope="col">Ссылка</th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($arr as $items): ?>
            <tr>
                <?php foreach ($items as $row): ?>
                    <td><?php echo $row; ?></td>
                <?php endforeach; ?>
            </tr>
        <?php endforeach; ?>
<!--<tr>-->
<!--    <td>gamebilla@gmail.com</td>-->
<!--    <td>Showtin Zhnak</td>-->
<!--    <td>12345678910111213</td>-->
<!--    <td>Пароль от пентагона не трогать!!!!!!!!</td>-->
<!--    <td>www.sai.eduself.com/addphp/link/href/nav/nav/nav/nav</td>-->
<!--</tr>-->
<!--<tr>-->
<!--    <td>Showtin Zhnak</td>-->
<!--    <td>Thornton</td>-->
<!--    <td>@fat</td>-->
<!--    <td>@twitter</td>-->
<!--    <td>@twitter</td>-->
<!--</tr>-->
<!--<tr>-->
<!--    <td>12345678910111213</td>-->
<!--    <td>the Bird</td>-->
<!--    <td>@twitter</td>-->
<!--    <td>@twitter</td>-->
        </tbody>
    </table>
</div>
</body>
</html> 